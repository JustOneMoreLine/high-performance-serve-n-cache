package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	
	router.GET("/read/:npm", readService)
	router.POST("/update", updateService)
	port := os.Getenv("HTTP_PORT")
	if port == "" {
		port = "8080"
	}
	println("Running on port " + port)
	log.Fatal(router.Run(":" + port))
}