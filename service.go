package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

var database = []UserNPM{
	{NPM: "1806235731", Name: "Muhammad Tsaqif Al Bari"},
	{NPM: "1606917632", Name: "Muzakki Hassan Azmi"},
	{NPM: "1806133774", Name: "Glenda Emanuella Sutanto"},
	{NPM: "1806186566", Name: "Rocky Arkan Adnan Ahmad"},
	{NPM: "1806191061", Name: "Irham Ilman Zhafir"},
	{NPM: "1806191704", Name: "Azkiya Hanna Rofifah"},
	{NPM: "1806204972", Name: "Kefas Satrio Bangkit Solideantyo"},
	{NPM: "1806205022", Name: "Louisa Natalika Jovanna"},
	{NPM: "1806205041", Name: "Timothy Regana Tarigan"},
	{NPM: "1806205123", Name: "Doan Andreas Nathanael"},
	{NPM: "1806205470", Name: "Muzaki Azami Khairevy"},
	{NPM: "1806205533", Name: "Muhammad Ilham Al Ghifari"},
	{NPM: "1806205722", Name: "Muhammad Rafif Elfazri"},
	{NPM: "1806205786", Name: "Willy Sandi Harsono"},
	{NPM: "1906285522", Name: "Julian Fernando"},
	{NPM: "1906285573", Name: "Muhammad Faishol Amirul Mukminin"},
	{NPM: "1906292894", Name: "Annisa Devi Nurmalasari"},
	{NPM: "1906292950", Name: "Azka Fitria"},
	{NPM: "1906292976", Name: "Calmeryan Jireh"},
	{NPM: "1906292995", Name: "Cornelita Lugita Santoso"},
	{NPM: "1906293120", Name: "Johanes Jason"},
	{NPM: "1906293190", Name: "Muhammad Fathan Muthahhari"},
	{NPM: "1906293253", Name: "Natasya Zahra"},
	{NPM: "1906293272", Name: "Novi Handayani"},
	{NPM: "1906293316", Name: "Shafira Putri Novia Hartanti"},
	{NPM: "1906305455", Name: "Alfina Megasiwi"},
	{NPM: "1906305871", Name: "Naufal Adi Wijanarko"},
	{NPM: "1906306640", Name: "Ahmad Haydar Alkaf"},
	{NPM: "1906306773", Name: "Ghifari Aulia Azhar Riza"},
	{NPM: "1906308066", Name: "Raja Aldwyn James Napintor Sihombing"},
	{NPM: "1906308305", Name: "Samuel Mulatua Jeremy"},
	{NPM: "1906308463", Name: "Yosua Immanuel Hutagalung"},
	{NPM: "1906308476", Name: "Johanes Marihot Perkasa Simarmata"},
	{NPM: "1906318924", Name: "Rheznandya Erwanto"},
	{NPM: "1906350521", Name: "William"},
	{NPM: "1906350566", Name: "I Gede Aditya Premana Putra"},
	{NPM: "1906350654", Name: "Muhamad Adamy Rayeuk"},
	{NPM: "1906350761", Name: "Farah Nazihah"},
	{NPM: "1906350830", Name: "Muhammad Ikhsan Asa Pambayun"},
	{NPM: "1906350881", Name: "Tsanaativa Vinnera"},
	{NPM: "1906398364", Name: "Rico Tadjudin"},
	{NPM: "1906398521", Name: "Arllivandhya Dani"},
	{NPM: "1906398534", Name: "Farzana Hadifa"},
	{NPM: "1906398540", Name: "Ahmadar Rafi Moreno"},
	{NPM: "1906398603", Name: "Rafi Muhammad"},
	{NPM: "1906398723", Name: "Naufal Sani"},
	{NPM: "1906400002", Name: "Muhammad Zaki"},
	{NPM: "1906400173", Name: "Alisha Yumna Bakri"},
	{NPM: "1906400186", Name: "Aimar Fikri Salafi"},
	{NPM: "1906400261", Name: "Jonathan Amadeus Hartman"},
	{NPM: "1706039793", Name: "Palito"},
	{NPM: "1706039982", Name: "Rhendy Rivaldo"},
	{NPM: "1806204915", Name: "Fredy Pasaud Marolan Silitonga"},
	{NPM: "1806204985", Name: "Jonathan"},
	{NPM: "1806205161", Name: "Jovi Handono Hutama"},
	{NPM: "1806205211", Name: "Ryan Karyadiputera"},
	{NPM: "1806205275", Name: "Darian Texanditama"},
	{NPM: "1806205413", Name: "Syams Ramadan"},
	{NPM: "1806205445", Name: "Evando Wihalim"},
	{NPM: "1806205571", Name: "Ryo Axtonlie"},
	{NPM: "1806235731", Name: "Muhammad Tsaqif Al Bari"},
	{NPM: "1906285472", Name: "Aldi Naufal Fitrah"},
	{NPM: "1906285516", Name: "Zulfan Kasilas Dinca"},
	{NPM: "1906285592", Name: "Samuel"},
	{NPM: "1906285604", Name: "Hocky Yudhiono"},
	{NPM: "1906285610", Name: "Fairuza Raryasdya Ayunda"},
	{NPM: "1906292906", Name: "Annisa Dian Nugrahani"},
	{NPM: "1906292912", Name: "Antonius Anggito Arissaputro"},
	{NPM: "1906292925", Name: "Ariasena Cahya Ramadhani"},
	{NPM: "1906292931", Name: "Aryaputra Athallah"},
	{NPM: "1906293013", Name: "Dewangga Putra Sheradhien"},
	{NPM: "1906293070", Name: "Hanif Arkan Audah"},
	{NPM: "1906293322", Name: "Steven"},
	{NPM: "1906306842", Name: "Naradhipa Mahardhika Setiawan Bhary"},
	{NPM: "1906307132", Name: "Fikri Akmal"},
	{NPM: "1906307145", Name: "Putu Wigunarta"},
	{NPM: "1906350616", Name: "Gani Ilham Irsyadi"},
	{NPM: "1906350774", Name: "Dionisius Baskoro Samudra"},
	{NPM: "1906350793", Name: "Vanessa Emily Agape"},
	{NPM: "1906350805", Name: "Muhammad Haikal Ubaidillah Susanto"},
	{NPM: "1906350824", Name: "Rafi Indrawan Dirgantara"},
	{NPM: "1906350843", Name: "Jafar Abdurrohman"},
	{NPM: "1906350862", Name: "Sae Pasomba"},
	{NPM: "1906350894", Name: "Niti Cahyaning Utami"},
	{NPM: "1906350925", Name: "Eko Julianto Salim"},
	{NPM: "1906350944", Name: "Asfiolitha Wilmarani"},
	{NPM: "1906350950", Name: "Muhammad Kenta Bisma Dewa Brata"},
	{NPM: "1906350963", Name: "Muhammad Hanif Anggawi"},
	{NPM: "1906398212", Name: "Ageng Anugrah Wardoyo Putra"},
	{NPM: "1906398225", Name: "Lazuardi P.p. Nusantara"},
	{NPM: "1906398250", Name: "Muhammad Alif Saddid"},
	{NPM: "1906398420", Name: "Nofaldi Fikrul Atmam"},
	{NPM: "1906398433", Name: "Muhammad Fayaad"},
	{NPM: "1906398484", Name: "Mario Serano"},
	{NPM: "1906398553", Name: "James Frederix Rolianto"},
	{NPM: "1906398566", Name: "Nadya Aprillia"},
	{NPM: "1906398572", Name: "Zulfahri Haradi Hidayatullah"},
	{NPM: "1906400160", Name: "Teofanus Gary Setiawan"},
	{NPM: "1906400280", Name: "Ian Andersen Ng"},
	{NPM: "1906400293", Name: "Faris Sayidinarechan Ardhafa"},
}

func readService(c *gin.Context) {
	// to simulate database loading
	time.Sleep(2 * time.Second)
	reqNpm := c.Param("npm")
	for _, usernpm := range database {
		if usernpm.NPM == reqNpm {
			result := Payload{
				Status: "OK",
				NPM: usernpm.NPM,
				Name: usernpm.Name,
			}
			c.IndentedJSON(http.StatusOK, result)
			return
		}
	}
	result := Payload{
		Status: "OK",
	}
	c.IndentedJSON(http.StatusOK, result)
	return
}

func updateService(c *gin.Context) {
	user := UserNPM{}
	c.BindJSON(&user)
	database = append(database, user)
	result := Response{
		Status: "OK",
	}
	c.IndentedJSON(http.StatusOK, result)
}

