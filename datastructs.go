package main

type UserNPM struct {
	NPM  string `json:"npm"`
	Name string `json:"nama"`
}

type Payload struct {
	Status string `json:"status"`
	NPM    string `json:"npm"`
	Name   string `json:"nama"`
}

type Response struct {
	Status string `json:"status"`
}